#include "cute.h"
#include "cstring.h"


string_t s;
const char *w;

string_t a;


void setUp(void)
{
    a = string_make(40);
    string_ccopy(s, "string");
}

void tearDown(void)
{
    string_delete(a);
}

void suiteSetUp(void)
{
    s = string_make(50);
    w = string_buf(s);
}

void suiteTearDown(void)
{
    string_delete(s);
}


void test_clone(void)
{
    string_t x;

    x = string_clone(s);
    TEST_ASSERT_STR_EQ(w, string_buf(x));
    TEST_ASSERT_ULONG_OP(==, 6UL, string_cap(x));
    TEST_ASSERT_ULONG_OP(==, 6UL, string_len(x));
    string_delete(x);

    x = string_cclone(w);
    TEST_ASSERT_STR_EQ(w, string_buf(x));
    TEST_ASSERT_ULONG_OP(==, 6UL, string_cap(x));
    TEST_ASSERT_ULONG_OP(==, 6UL, string_len(x));
    string_delete(x);

    string_reset(s);

    x = string_cclone(w);
    TEST_ASSERT_STR_EQ(w, string_buf(x));
    TEST_ASSERT_ULONG_OP(==, 0UL, string_cap(x));
    TEST_ASSERT_ULONG_OP(==, 0UL, string_len(x));
    string_delete(x);

    TEST_ASSERT_NULL(string_clone(NULL));
    TEST_ASSERT_NULL(string_cclone(NULL));
}

void test_copy(void)
{
    a = string_copy(a, s);
    TEST_ASSERT_STR_EQ( w, string_buf(a));
    TEST_ASSERT_ULONG_OP(==, 40UL, string_cap(a));
    TEST_ASSERT_ULONG_OP(==,  6UL, string_len(a));

    a = string_ccopy(a, w);
    TEST_ASSERT_STR_EQ( w, string_buf(a));
    TEST_ASSERT_ULONG_OP(==, 40UL, string_cap(a));
    TEST_ASSERT_ULONG_OP(==,  6UL, string_len(a));

    string_reset(s);

    a = string_ccopy(a, w);
    TEST_ASSERT_STR_EQ( w, string_buf(a));
    TEST_ASSERT_ULONG_OP(==, 40UL, string_cap(a));
    TEST_ASSERT_ULONG_OP(==,  0UL, string_len(a));

    TEST_ASSERT_PTR_EQ(a, string_copy(a, NULL));
    TEST_ASSERT_PTR_EQ(a, string_ccopy(a, NULL));
}

void test_append(void)
{
    a = string_ccopy(a, "xxx ");
    a = string_append(a, s);
    TEST_ASSERT_STR_EQ("xxx string", string_buf(a));
    TEST_ASSERT_ULONG_OP(==, 40UL, string_cap(a));
    TEST_ASSERT_ULONG_OP(==, 10UL, string_len(a));

    a = string_ccopy(a, "yyy ");
    a = string_cappend(a, w);
    TEST_ASSERT_STR_EQ("yyy string", string_buf(a));
    TEST_ASSERT_ULONG_OP(==, 40UL, string_cap(a));
    TEST_ASSERT_ULONG_OP(==, 10UL, string_len(a));

    string_reset(s);

    a = string_ccopy(a, "yyy ");
    a = string_cappend(a, w);
    TEST_ASSERT_STR_EQ("yyy ", string_buf(a));
    TEST_ASSERT_ULONG_OP(==, 40UL, string_cap(a));
    TEST_ASSERT_ULONG_OP(==,  4UL, string_len(a));

    TEST_ASSERT_PTR_EQ(a, string_append(a, NULL));
    TEST_ASSERT_PTR_EQ(a, string_cappend(a, NULL));
}

void test_prepend(void)
{
    a = string_ccopy(a, " xxx");
    a = string_prepend(a, s);
    TEST_ASSERT_STR_EQ("string xxx", string_buf(a));
    TEST_ASSERT_ULONG_OP(==, 40UL, string_cap(a));
    TEST_ASSERT_ULONG_OP(==, 10UL, string_len(a));

    a = string_ccopy(a, " yyy");
    a = string_cprepend(a, w);
    TEST_ASSERT_STR_EQ("string yyy", string_buf(a));
    TEST_ASSERT_ULONG_OP(==, 40UL, string_cap(a));
    TEST_ASSERT_ULONG_OP(==, 10UL, string_len(a));

    string_reset(s);

    a = string_ccopy(a, " yyy");
    a = string_cprepend(a, w);
    TEST_ASSERT_STR_EQ(" yyy", string_buf(a));
    TEST_ASSERT_ULONG_OP(==, 40UL, string_cap(a));
    TEST_ASSERT_ULONG_OP(==,  4UL, string_len(a));

    TEST_ASSERT_PTR_EQ(a, string_prepend(a, NULL));
    TEST_ASSERT_PTR_EQ(a, string_cprepend(a, NULL));
}

void test_join(void)
{
    const string_t sarr[] = {
        string_cclone("The"),
        string_cclone("Quick"),
        string_cclone("Brown"),
        string_cclone("Fox")
    };
    const char *carr[] = {
        "Jumps",
        "Over",
        "The",
        "Dog"
    };

    a = string_join(a, sarr, 4, " ");
    TEST_ASSERT_STR_EQ("The Quick Brown Fox", string_buf(a));

    a = string_cjoin(a, carr, 4, " ");
    TEST_ASSERT_STR_EQ("Jumps Over The Dog", string_buf(a));

    a = string_join(a, sarr, 4, " / ");
    TEST_ASSERT_STR_EQ("The / Quick / Brown / Fox", string_buf(a));

    a = string_cjoin(a, carr, 4, " / ");
    TEST_ASSERT_STR_EQ("Jumps / Over / The / Dog", string_buf(a));

    a = string_join(a, sarr, 4, "");
    TEST_ASSERT_STR_EQ("TheQuickBrownFox", string_buf(a));

    a = string_cjoin(a, carr, 4, "");
    TEST_ASSERT_STR_EQ("JumpsOverTheDog", string_buf(a));

    a = string_join(a, sarr, 2, " | ");
    TEST_ASSERT_STR_EQ("The | Quick", string_buf(a));

    a = string_cjoin(a, carr, 2, " | ");
    TEST_ASSERT_STR_EQ("Jumps | Over", string_buf(a));

    a = string_join(a, sarr, 1, " | ");
    TEST_ASSERT_STR_EQ("The", string_buf(a));

    a = string_cjoin(a, carr, 1, " | ");
    TEST_ASSERT_STR_EQ("Jumps", string_buf(a));

    TEST_ASSERT_PTR_EQ(a, string_join(a, sarr, 0, " | "));
    TEST_ASSERT_PTR_EQ(a, string_cjoin(a, carr, 0, " | "));

    string_delete(sarr[0]);
    string_delete(sarr[1]);
    string_delete(sarr[2]);
    string_delete(sarr[3]);
}

void test_replace(void)
{
    a = string_ccopy(a, "The Quick Brown Fox");
    a = string_creplace(a, "Fox", "Dog");
    TEST_ASSERT_STR_EQ("The Quick Brown Dog", string_buf(a));
    TEST_ASSERT_ULONG_OP(==, 19UL, string_len(a));

    a = string_creplace(a, "The Quick Brown Dog", "A Slow Green Turtle");
    TEST_ASSERT_STR_EQ("A Slow Green Turtle", string_buf(a));
    TEST_ASSERT_ULONG_OP(==, 19UL, string_len(a));

    a = string_creplace(a, " ", ", ");
    TEST_ASSERT_STR_EQ("A, Slow, Green, Turtle", string_buf(a));
    TEST_ASSERT_ULONG_OP(==, 22UL, string_len(a));

    a = string_ccopy(a, "kayak kayak kayak");
    a = string_creplace(a, "aya", "");
    TEST_ASSERT_STR_EQ("kk kk kk", string_buf(a));
    TEST_ASSERT_ULONG_OP(==, 8UL, string_len(a));

    a = string_creplace(a, "kk ", "");
    TEST_ASSERT_STR_EQ("kk", string_buf(a));
    TEST_ASSERT_ULONG_OP(==, 2UL, string_len(a));

    a = string_creplace(a, "k", "");
    TEST_ASSERT_STR_EQ("", string_buf(a));
    TEST_ASSERT_ULONG_OP(==, 0UL, string_len(a));
    TEST_ASSERT(string_isempty(a));

    TEST_ASSERT_PTR_EQ(a, string_creplace(a, NULL, NULL));
    TEST_ASSERT_PTR_EQ(a, string_replace(a, NULL, NULL));
}


int main(void)
{
    TEST_BEGIN();

    TEST_RUN(test_clone);
    TEST_RUN(test_copy);
    TEST_RUN(test_append);
    TEST_RUN(test_prepend);
    TEST_RUN(test_join);
    TEST_RUN(test_replace);

    return TEST_END();
}
