#include "cute.h"
#include "cstring.h"


string_t a, b;


void setUp(void)
{
    a = string_cclone("Hello World");
    b = string_cclone("()[]{} 0369 +-*/=");
}

void tearDown(void)
{
    string_delete(a);
    string_delete(b);
}

void suiteSetUp(void)
{
}

void suiteTearDown(void)
{
}


void test_reset(void)
{
    string_reset(a);
    TEST_ASSERT_STR_EQ("", string_buf(a));
    TEST_ASSERT_ULONG_OP(==,  0UL, string_len(a));
    TEST_ASSERT_ULONG_OP(==, 11UL, string_cap(a));
}

void test_toupper(void)
{
    TEST_ASSERT_STR_EQ("HELLO WORLD", string_buf(string_toupper(a)));
    TEST_ASSERT_STR_EQ("()[]{} 0369 +-*/=", string_buf(string_toupper(b)));
}

void test_tolower(void)
{
    TEST_ASSERT_STR_EQ("hello world", string_buf(string_tolower(a)));
    TEST_ASSERT_STR_EQ("()[]{} 0369 +-*/=", string_buf(string_toupper(b)));
}


int main(void)
{
    TEST_BEGIN();

    TEST_RUN(test_reset);
    TEST_RUN(test_toupper);
    TEST_RUN(test_tolower);

    return TEST_END();
}
