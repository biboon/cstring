#include "cute.h"
#include "cstring.h"


void setUp(void)
{
    // Setup before test
}

void tearDown(void)
{
    // Cleanup after test
}

void suiteSetUp(void)
{
}

void suiteTearDown(void)
{
}


void test_make(void)
{
    for (size_t len = 1000; len > 0; len /= 10)
    {
        string_t s = string_make(0);
        TEST_ASSERT_NOT_NULL(s);
        string_delete(s);
    }
}


void test_delete_null(void)
{
    string_delete(NULL);
}

void test_delete_non_null(void)
{
    string_t s = string_make(10);
    TEST_ASSERT_NOT_NULL(s);
    string_delete(s);
}


void test_grow_from_null(void)
{
    TEST_ASSERT_NULL(string_grow(NULL, 42));
}

void test_grow_from_zero(void)
{
    string_t s = string_make(0);

    TEST_ASSERT_PTR_EQ(s, string_grow(s, 0));
    TEST_ASSERT_ULONG_OP(==, 0UL, string_cap(s));
    TEST_ASSERT_ULONG_OP(==, 0UL, string_len(s));

    s = string_grow(s, 131);
    TEST_ASSERT_ULONG_OP(==, 131UL, string_cap(s));
    TEST_ASSERT_ULONG_OP(==, 0UL, string_len(s));

    string_delete(s);
}

void test_grow_from_non_zero(void)
{
    string_t s = string_make(131);

    TEST_ASSERT_PTR_EQ(s, string_grow(s, 0));
    TEST_ASSERT_ULONG_OP(==, 131UL, string_cap(s));
    TEST_ASSERT_ULONG_OP(==, 0UL, string_len(s));

    TEST_ASSERT_PTR_EQ(s, string_grow(s, 8));
    TEST_ASSERT_ULONG_OP(==, 131UL, string_cap(s));
    TEST_ASSERT_ULONG_OP(==, 0UL, string_len(s));

    TEST_ASSERT_PTR_EQ(s, string_grow(s, 131));
    TEST_ASSERT_ULONG_OP(==, 131UL, string_cap(s));
    TEST_ASSERT_ULONG_OP(==, 0UL, string_len(s));

    s = string_grow(s, 150);
    TEST_ASSERT_ULONG_OP(==, 262UL, string_cap(s));
    TEST_ASSERT_ULONG_OP(==, 0UL, string_len(s));

    string_delete(s);
}

void test_grow_to_next_size(void)
{
    string_t s = string_make(131);

    s = string_grow(s, 262);
    TEST_ASSERT_ULONG_OP(==, 262UL, string_cap(s));
    TEST_ASSERT_ULONG_OP(==, 0UL, string_len(s));

    s = string_grow(s, 524);
    TEST_ASSERT_ULONG_OP(==, 524UL, string_cap(s));
    TEST_ASSERT_ULONG_OP(==, 0UL, string_len(s));

    string_delete(s);
}


int main(void)
{
    TEST_BEGIN();

    TEST_RUN(test_make);
    TEST_RUN(test_delete_null);
    TEST_RUN(test_delete_non_null);
    TEST_RUN(test_grow_from_null);
    TEST_RUN(test_grow_from_zero);
    TEST_RUN(test_grow_from_non_zero);
    TEST_RUN(test_grow_to_next_size);

    return TEST_END();
}
