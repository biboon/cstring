#include "cute.h"
#include "cstring.h"


string_t s1, s2, s3;


void setUp(void)
{
    s1 = string_cclone("aaabbbcccdddeee");
    s2 = string_clone(s1);
    s3 = string_clone(s1);
}

void tearDown(void)
{
    string_delete(s1);
    string_delete(s2);
    string_delete(s3);
}

void suiteSetUp(void)
{
}

void suiteTearDown(void)
{
}


void test_trimming_a(void)
{
    s1 = string_trimleft(s1, "a");
    TEST_ASSERT_STR_EQ("bbbcccdddeee", string_buf(s1));
    TEST_ASSERT_ULONG_OP(==, 12UL, string_len(s1));
    TEST_ASSERT_ULONG_OP(==, 15UL, string_cap(s1));

    s2 = string_trimright(s2, "a");
    TEST_ASSERT_STR_EQ("aaabbbcccdddeee", string_buf(s2));
    TEST_ASSERT_ULONG_OP(==, 15UL, string_len(s2));
    TEST_ASSERT_ULONG_OP(==, 15UL, string_cap(s2));

    s3 = string_trim(s3, "a");
    TEST_ASSERT_STR_EQ("bbbcccdddeee", string_buf(s3));
    TEST_ASSERT_ULONG_OP(==, 12UL, string_len(s3));
    TEST_ASSERT_ULONG_OP(==, 15UL, string_cap(s3));
}

void test_trimming_b(void)
{
    s1 = string_trimleft(s1, "b");
    TEST_ASSERT_STR_EQ("aaabbbcccdddeee", string_buf(s1));
    TEST_ASSERT_ULONG_OP(==, 15UL, string_len(s1));
    TEST_ASSERT_ULONG_OP(==, 15UL, string_cap(s1));

    s2 = string_trimright(s2, "b");
    TEST_ASSERT_STR_EQ("aaabbbcccdddeee", string_buf(s2));
    TEST_ASSERT_ULONG_OP(==, 15UL, string_len(s2));
    TEST_ASSERT_ULONG_OP(==, 15UL, string_cap(s2));

    s3 = string_trim(s3, "b");
    TEST_ASSERT_STR_EQ("aaabbbcccdddeee", string_buf(s3));
    TEST_ASSERT_ULONG_OP(==, 15UL, string_len(s3));
    TEST_ASSERT_ULONG_OP(==, 15UL, string_cap(s3));
}

void test_trimming_ab(void)
{
    s1 = string_trimleft(s1, "ab");
    TEST_ASSERT_STR_EQ("cccdddeee", string_buf(s1));
    TEST_ASSERT_ULONG_OP(==, 9UL, string_len(s1));
    TEST_ASSERT_ULONG_OP(==, 15UL, string_cap(s1));

    s2 = string_trimright(s2, "ab");
    TEST_ASSERT_STR_EQ("aaabbbcccdddeee", string_buf(s2));
    TEST_ASSERT_ULONG_OP(==, 15UL, string_len(s2));
    TEST_ASSERT_ULONG_OP(==, 15UL, string_cap(s2));

    s3 = string_trim(s3, "ab");
    TEST_ASSERT_STR_EQ("cccdddeee", string_buf(s3));
    TEST_ASSERT_ULONG_OP(==, 9UL, string_len(s3));
    TEST_ASSERT_ULONG_OP(==, 15UL, string_cap(s3));
}

void test_trimming_ac(void)
{
    s1 = string_trimleft(s1, "ac");
    TEST_ASSERT_STR_EQ("bbbcccdddeee", string_buf(s1));
    TEST_ASSERT_ULONG_OP(==, 12UL, string_len(s1));
    TEST_ASSERT_ULONG_OP(==, 15UL, string_cap(s1));

    s2 = string_trimright(s2, "ac");
    TEST_ASSERT_STR_EQ("aaabbbcccdddeee", string_buf(s2));
    TEST_ASSERT_ULONG_OP(==, 15UL, string_len(s2));
    TEST_ASSERT_ULONG_OP(==, 15UL, string_cap(s2));

    s3 = string_trim(s3, "ac");
    TEST_ASSERT_STR_EQ("bbbcccdddeee", string_buf(s3));
    TEST_ASSERT_ULONG_OP(==, 12UL, string_len(s3));
    TEST_ASSERT_ULONG_OP(==, 15UL, string_cap(s3));
}

void test_trimming_e(void)
{
    s1 = string_trimleft(s1, "e");
    TEST_ASSERT_STR_EQ("aaabbbcccdddeee", string_buf(s1));
    TEST_ASSERT_ULONG_OP(==, 15UL, string_len(s1));
    TEST_ASSERT_ULONG_OP(==, 15UL, string_cap(s1));

    s2 = string_trimright(s2, "e");
    TEST_ASSERT_STR_EQ("aaabbbcccddd", string_buf(s2));
    TEST_ASSERT_ULONG_OP(==, 12UL, string_len(s2));
    TEST_ASSERT_ULONG_OP(==, 15UL, string_cap(s2));

    s3 = string_trim(s3, "e");
    TEST_ASSERT_STR_EQ("aaabbbcccddd", string_buf(s3));
    TEST_ASSERT_ULONG_OP(==, 12UL, string_len(s3));
    TEST_ASSERT_ULONG_OP(==, 15UL, string_cap(s3));
}

void test_trimming_d(void)
{
    s1 = string_trimleft(s1, "d");
    TEST_ASSERT_STR_EQ("aaabbbcccdddeee", string_buf(s1));
    TEST_ASSERT_ULONG_OP(==, 15UL, string_len(s1));
    TEST_ASSERT_ULONG_OP(==, 15UL, string_cap(s1));

    s2 = string_trimright(s2, "d");
    TEST_ASSERT_STR_EQ("aaabbbcccdddeee", string_buf(s2));
    TEST_ASSERT_ULONG_OP(==, 15UL, string_len(s2));
    TEST_ASSERT_ULONG_OP(==, 15UL, string_cap(s2));

    s3 = string_trim(s3, "d");
    TEST_ASSERT_STR_EQ("aaabbbcccdddeee", string_buf(s3));
    TEST_ASSERT_ULONG_OP(==, 15UL, string_len(s3));
    TEST_ASSERT_ULONG_OP(==, 15UL, string_cap(s3));
}

void test_trimming_de(void)
{
    s1 = string_trimleft(s1, "de");
    TEST_ASSERT_STR_EQ("aaabbbcccdddeee", string_buf(s1));
    TEST_ASSERT_ULONG_OP(==, 15UL, string_len(s1));
    TEST_ASSERT_ULONG_OP(==, 15UL, string_cap(s1));

    s2 = string_trimright(s2, "de");
    TEST_ASSERT_STR_EQ("aaabbbccc", string_buf(s2));
    TEST_ASSERT_ULONG_OP(==, 9UL, string_len(s2));
    TEST_ASSERT_ULONG_OP(==, 15UL, string_cap(s2));

    s3 = string_trim(s3, "de");
    TEST_ASSERT_STR_EQ("aaabbbccc", string_buf(s3));
    TEST_ASSERT_ULONG_OP(==, 9UL, string_len(s3));
    TEST_ASSERT_ULONG_OP(==, 15UL, string_cap(s3));
}

void test_trimming_ce(void)
{
    s1 = string_trimleft(s1, "ce");
    TEST_ASSERT_STR_EQ("aaabbbcccdddeee", string_buf(s1));
    TEST_ASSERT_ULONG_OP(==, 15UL, string_len(s1));
    TEST_ASSERT_ULONG_OP(==, 15UL, string_cap(s1));

    s2 = string_trimright(s2, "ce");
    TEST_ASSERT_STR_EQ("aaabbbcccddd", string_buf(s2));
    TEST_ASSERT_ULONG_OP(==, 12UL, string_len(s2));
    TEST_ASSERT_ULONG_OP(==, 15UL, string_cap(s2));

    s3 = string_trim(s3, "ce");
    TEST_ASSERT_STR_EQ("aaabbbcccddd", string_buf(s3));
    TEST_ASSERT_ULONG_OP(==, 12UL, string_len(s3));
    TEST_ASSERT_ULONG_OP(==, 15UL, string_cap(s3));
}

void test_trimming_ae(void)
{
    s1 = string_trimleft(s1, "ae");
    TEST_ASSERT_STR_EQ("bbbcccdddeee", string_buf(s1));
    TEST_ASSERT_ULONG_OP(==, 12UL, string_len(s1));
    TEST_ASSERT_ULONG_OP(==, 15UL, string_cap(s1));

    s2 = string_trimright(s2, "ae");
    TEST_ASSERT_STR_EQ("aaabbbcccddd", string_buf(s2));
    TEST_ASSERT_ULONG_OP(==, 12UL, string_len(s2));
    TEST_ASSERT_ULONG_OP(==, 15UL, string_cap(s2));

    s3 = string_trim(s3, "ae");
    TEST_ASSERT_STR_EQ("bbbcccddd", string_buf(s3));
    TEST_ASSERT_ULONG_OP(==, 9UL, string_len(s3));
    TEST_ASSERT_ULONG_OP(==, 15UL, string_cap(s3));
}

void test_trimming_bd(void)
{
    s1 = string_trimleft(s1, "bd");
    TEST_ASSERT_STR_EQ("aaabbbcccdddeee", string_buf(s1));
    TEST_ASSERT_ULONG_OP(==, 15UL, string_len(s1));
    TEST_ASSERT_ULONG_OP(==, 15UL, string_cap(s1));

    s2 = string_trimright(s2, "bd");
    TEST_ASSERT_STR_EQ("aaabbbcccdddeee", string_buf(s2));
    TEST_ASSERT_ULONG_OP(==, 15UL, string_len(s2));
    TEST_ASSERT_ULONG_OP(==, 15UL, string_cap(s2));

    s3 = string_trim(s3, "bd");
    TEST_ASSERT_STR_EQ("aaabbbcccdddeee", string_buf(s3));
    TEST_ASSERT_ULONG_OP(==, 15UL, string_len(s3));
    TEST_ASSERT_ULONG_OP(==, 15UL, string_cap(s3));
}

void test_trimming_abde(void)
{
    s1 = string_trimleft(s1, "abde");
    TEST_ASSERT_STR_EQ("cccdddeee", string_buf(s1));
    TEST_ASSERT_ULONG_OP(==, 9UL, string_len(s1));
    TEST_ASSERT_ULONG_OP(==, 15UL, string_cap(s1));

    s2 = string_trimright(s2, "abde");
    TEST_ASSERT_STR_EQ("aaabbbccc", string_buf(s2));
    TEST_ASSERT_ULONG_OP(==, 9UL, string_len(s2));
    TEST_ASSERT_ULONG_OP(==, 15UL, string_cap(s2));

    s3 = string_trim(s3, "abde");
    TEST_ASSERT_STR_EQ("ccc", string_buf(s3));
    TEST_ASSERT_ULONG_OP(==, 3UL, string_len(s3));
    TEST_ASSERT_ULONG_OP(==, 15UL, string_cap(s3));
}

void test_trimming_ace(void)
{
    s1 = string_trimleft(s1, "ace");
    TEST_ASSERT_STR_EQ("bbbcccdddeee", string_buf(s1));
    TEST_ASSERT_ULONG_OP(==, 12UL, string_len(s1));
    TEST_ASSERT_ULONG_OP(==, 15UL, string_cap(s1));

    s2 = string_trimright(s2, "ace");
    TEST_ASSERT_STR_EQ("aaabbbcccddd", string_buf(s2));
    TEST_ASSERT_ULONG_OP(==, 12UL, string_len(s2));
    TEST_ASSERT_ULONG_OP(==, 15UL, string_cap(s2));

    s3 = string_trim(s3, "ace");
    TEST_ASSERT_STR_EQ("bbbcccddd", string_buf(s3));
    TEST_ASSERT_ULONG_OP(==, 9UL, string_len(s3));
    TEST_ASSERT_ULONG_OP(==, 15UL, string_cap(s3));
}

void test_trimming_abcde(void)
{
    s1 = string_trimleft(s1, "abcde");
    TEST_ASSERT_STR_EQ("", string_buf(s1));
    TEST_ASSERT_ULONG_OP(==, 0UL, string_len(s1));
    TEST_ASSERT_ULONG_OP(==, 15UL, string_cap(s1));

    s2 = string_trimright(s2, "abcde");
    TEST_ASSERT_STR_EQ("", string_buf(s2));
    TEST_ASSERT_ULONG_OP(==, 0UL, string_len(s2));
    TEST_ASSERT_ULONG_OP(==, 15UL, string_cap(s2));

    s3 = string_trim(s3, "abcde");
    TEST_ASSERT_STR_EQ("", string_buf(s3));
    TEST_ASSERT_ULONG_OP(==, 0UL, string_len(s3));
    TEST_ASSERT_ULONG_OP(==, 15UL, string_cap(s3));
}

void test_trimming_spaces(void)
{
    s1 = string_ccopy(s1, "\tThe Quick Brown Fox   \r\n");
    s1 = string_trimspace(s1);
    TEST_ASSERT_STR_EQ("The Quick Brown Fox", string_buf(s1));
}


int main(void)
{
    TEST_BEGIN();

    TEST_RUN(test_trimming_a);
    TEST_RUN(test_trimming_b);
    TEST_RUN(test_trimming_ab);
    TEST_RUN(test_trimming_ac);
    TEST_RUN(test_trimming_e);
    TEST_RUN(test_trimming_d);
    TEST_RUN(test_trimming_de);
    TEST_RUN(test_trimming_ce);
    TEST_RUN(test_trimming_ae);
    TEST_RUN(test_trimming_bd);
    TEST_RUN(test_trimming_abde);
    TEST_RUN(test_trimming_ace);
    TEST_RUN(test_trimming_abcde);
    TEST_RUN(test_trimming_spaces);

    return TEST_END();
}
