#include "cute.h"
#include "cstring.h"


string_t a, b;


void setUp(void)
{
    a = string_cclone("aaa");
    b = string_cclone("bbb");
}

void tearDown(void)
{
    string_delete(a);
    string_delete(b);
}

void suiteSetUp(void)
{
}

void suiteTearDown(void)
{
}


void test_compare(void)
{
    TEST_ASSERT_INT_OP(==,  0, string_compare(a, a));
    TEST_ASSERT_INT_OP(==, -1, string_compare(a, b));
    TEST_ASSERT_INT_OP(==,  1, string_compare(b, a));
    TEST_ASSERT_INT_OP(==,  0, string_compare(b, b));

    TEST_ASSERT_INT_OP(==,  0, string_ccompare(a, "aaa"));
    TEST_ASSERT_INT_OP(==, -1, string_ccompare(a, "bbb"));
    TEST_ASSERT_INT_OP(==,  1, string_ccompare(b, "aaa"));
    TEST_ASSERT_INT_OP(==,  0, string_ccompare(b, "bbb"));
}

void test_equals(void)
{
    TEST_ASSERT_TRUE (string_equals(a, a));
    TEST_ASSERT_FALSE(string_equals(a, b));
    TEST_ASSERT_FALSE(string_equals(b, a));
    TEST_ASSERT_TRUE (string_equals(b, b));

    TEST_ASSERT_TRUE (string_cequals(a, "aaa"));
    TEST_ASSERT_FALSE(string_cequals(a, "bbb"));
    TEST_ASSERT_FALSE(string_cequals(b, "aaa"));
    TEST_ASSERT_TRUE (string_cequals(b, "bbb"));
}

void test_isempty(void)
{
    TEST_ASSERT_FALSE(string_isempty(a));
    a = string_ccopy(a, "");
    TEST_ASSERT_TRUE(string_isempty(a));

    TEST_ASSERT_FALSE(string_isempty(b));
    b = string_reset(b);
    TEST_ASSERT_TRUE(string_isempty(b));
}


int main(void)
{
    TEST_BEGIN();

    TEST_RUN(test_compare);
    TEST_RUN(test_equals);
    TEST_RUN(test_isempty);

    return TEST_END();
}
