#include "cute.h"
#include "cstring.h"


string_t str, ws;


void setUp(void)
{
    str = string_cclone("kayak kayak");
    ws  = string_make(20);
}

void tearDown(void)
{
    string_delete(str);
    string_delete(ws);
}

void suiteSetUp(void)
{
}

void suiteTearDown(void)
{
}


void test_index_k(void)
{
    const char *wc = "k";

    ws = string_ccopy(ws, wc);

    TEST_ASSERT_INT_OP(==,  0, string_index(str, ws));
    TEST_ASSERT_INT_OP(==,  0, string_cindex(str, wc));
    TEST_ASSERT_INT_OP(==, 10, string_lastindex(str, ws));
    TEST_ASSERT_INT_OP(==, 10, string_clastindex(str, wc));
    TEST_ASSERT_INT_OP(==,  4, string_count(str, ws));
    TEST_ASSERT_INT_OP(==,  4, string_ccount(str, wc));

    TEST_ASSERT_TRUE(string_hasprefix(str, ws));
    TEST_ASSERT_TRUE(string_chasprefix(str, wc));
    TEST_ASSERT_TRUE(string_hassuffix(str, ws));
    TEST_ASSERT_TRUE(string_chassuffix(str, wc));
    TEST_ASSERT_TRUE(string_contains(str, ws));
    TEST_ASSERT_TRUE(string_ccontains(str, wc));
}

void test_index_a(void)
{
    const char *wc = "a";

    ws = string_ccopy(ws, wc);

    TEST_ASSERT_INT_OP(==, 1, string_index(str, ws));
    TEST_ASSERT_INT_OP(==, 1, string_cindex(str, wc));
    TEST_ASSERT_INT_OP(==, 9, string_lastindex(str, ws));
    TEST_ASSERT_INT_OP(==, 9, string_clastindex(str, wc));
    TEST_ASSERT_INT_OP(==, 4, string_count(str, ws));
    TEST_ASSERT_INT_OP(==, 4, string_ccount(str, wc));

    TEST_ASSERT_FALSE(string_hasprefix(str, ws));
    TEST_ASSERT_FALSE(string_chasprefix(str, wc));
    TEST_ASSERT_FALSE(string_hassuffix(str, ws));
    TEST_ASSERT_FALSE(string_chassuffix(str, wc));
    TEST_ASSERT_TRUE (string_contains(str, ws));
    TEST_ASSERT_TRUE (string_ccontains(str, wc));
}

void test_index_kay(void)
{
    const char *wc = "kay";

    ws = string_ccopy(ws, wc);

    TEST_ASSERT_INT_OP(==, 0, string_index(str, ws));
    TEST_ASSERT_INT_OP(==, 0, string_cindex(str, wc));
    TEST_ASSERT_INT_OP(==, 6, string_lastindex(str, ws));
    TEST_ASSERT_INT_OP(==, 6, string_clastindex(str, wc));
    TEST_ASSERT_INT_OP(==, 2, string_count(str, ws));
    TEST_ASSERT_INT_OP(==, 2, string_ccount(str, wc));

    TEST_ASSERT_TRUE (string_hasprefix(str, ws));
    TEST_ASSERT_TRUE (string_chasprefix(str, wc));
    TEST_ASSERT_FALSE(string_hassuffix(str, ws));
    TEST_ASSERT_FALSE(string_chassuffix(str, wc));
    TEST_ASSERT_TRUE (string_contains(str, ws));
    TEST_ASSERT_TRUE (string_ccontains(str, wc));
}

void test_index_ak_ka(void)
{
    const char *wc = "ak ka";

    ws = string_ccopy(ws, wc);

    TEST_ASSERT_INT_OP(==, 3, string_index(str, ws));
    TEST_ASSERT_INT_OP(==, 3, string_cindex(str, wc));
    TEST_ASSERT_INT_OP(==, 3, string_lastindex(str, ws));
    TEST_ASSERT_INT_OP(==, 3, string_clastindex(str, wc));
    TEST_ASSERT_INT_OP(==, 1, string_count(str, ws));
    TEST_ASSERT_INT_OP(==, 1, string_ccount(str, wc));

    TEST_ASSERT_FALSE(string_hasprefix(str, ws));
    TEST_ASSERT_FALSE(string_chasprefix(str, wc));
    TEST_ASSERT_FALSE(string_hassuffix(str, ws));
    TEST_ASSERT_FALSE(string_chassuffix(str, wc));
    TEST_ASSERT_TRUE (string_contains(str, ws));
    TEST_ASSERT_TRUE (string_ccontains(str, wc));
}

void test_index_boat(void)
{
    const char *wc = "boat";

    ws = string_ccopy(ws, wc);

    TEST_ASSERT_INT_OP(==, -1, string_index(str, ws));
    TEST_ASSERT_INT_OP(==, -1, string_cindex(str, wc));
    TEST_ASSERT_INT_OP(==, -1, string_lastindex(str, ws));
    TEST_ASSERT_INT_OP(==, -1, string_clastindex(str, wc));
    TEST_ASSERT_INT_OP(==,  0, string_count(str, ws));
    TEST_ASSERT_INT_OP(==,  0, string_ccount(str, wc));

    TEST_ASSERT_FALSE(string_hasprefix(str, ws));
    TEST_ASSERT_FALSE(string_chasprefix(str, wc));
    TEST_ASSERT_FALSE(string_hassuffix(str, ws));
    TEST_ASSERT_FALSE(string_chassuffix(str, wc));
    TEST_ASSERT_FALSE(string_contains(str, ws));
    TEST_ASSERT_FALSE(string_ccontains(str, wc));
}

void test_index_arobase(void)
{
    const char *wc = "@";

    ws = string_ccopy(ws, wc);

    TEST_ASSERT_INT_OP(==, -1, string_index(str, ws));
    TEST_ASSERT_INT_OP(==, -1, string_cindex(str, wc));
    TEST_ASSERT_INT_OP(==, -1, string_lastindex(str, ws));
    TEST_ASSERT_INT_OP(==, -1, string_clastindex(str, wc));
    TEST_ASSERT_INT_OP(==,  0, string_count(str, ws));
    TEST_ASSERT_INT_OP(==,  0, string_ccount(str, wc));

    TEST_ASSERT_FALSE(string_hasprefix(str, ws));
    TEST_ASSERT_FALSE(string_chasprefix(str, wc));
    TEST_ASSERT_FALSE(string_hassuffix(str, ws));
    TEST_ASSERT_FALSE(string_chassuffix(str, wc));
    TEST_ASSERT_FALSE(string_contains(str, ws));
    TEST_ASSERT_FALSE(string_ccontains(str, wc));
}

void test_index_same_string(void)
{
    const char *wc = string_buf(str);

    ws = string_ccopy(ws, wc);

    TEST_ASSERT_INT_OP(==, 0, string_index(str, ws));
    TEST_ASSERT_INT_OP(==, 0, string_cindex(str, wc));
    TEST_ASSERT_INT_OP(==, 0, string_lastindex(str, ws));
    TEST_ASSERT_INT_OP(==, 0, string_clastindex(str, wc));
    TEST_ASSERT_INT_OP(==, 1, string_count(str, ws));
    TEST_ASSERT_INT_OP(==, 1, string_ccount(str, wc));

    TEST_ASSERT_TRUE(string_hasprefix(str, ws));
    TEST_ASSERT_TRUE(string_chasprefix(str, wc));
    TEST_ASSERT_TRUE(string_hassuffix(str, ws));
    TEST_ASSERT_TRUE(string_chassuffix(str, wc));
    TEST_ASSERT_TRUE(string_contains(str, ws));
    TEST_ASSERT_TRUE(string_ccontains(str, wc));
}

void test_index_same_length_different_string(void)
{
    const char *wc = "cayac cayac";

    ws = string_ccopy(ws, wc);

    TEST_ASSERT_INT_OP(==, -1, string_index(str, ws));
    TEST_ASSERT_INT_OP(==, -1, string_cindex(str, wc));
    TEST_ASSERT_INT_OP(==, -1, string_lastindex(str, ws));
    TEST_ASSERT_INT_OP(==, -1, string_clastindex(str, wc));
    TEST_ASSERT_INT_OP(==,  0, string_count(str, ws));
    TEST_ASSERT_INT_OP(==,  0, string_ccount(str, wc));

    TEST_ASSERT_FALSE(string_hasprefix(str, ws));
    TEST_ASSERT_FALSE(string_chasprefix(str, wc));
    TEST_ASSERT_FALSE(string_hassuffix(str, ws));
    TEST_ASSERT_FALSE(string_chassuffix(str, wc));
    TEST_ASSERT_FALSE(string_contains(str, ws));
    TEST_ASSERT_FALSE(string_ccontains(str, wc));
}

void test_index_3kayak(void)
{
    const char *wc = "kayak kayak kayak";

    ws = string_ccopy(ws, wc);

    TEST_ASSERT_INT_OP(==, -1, string_index(str, ws));
    TEST_ASSERT_INT_OP(==, -1, string_cindex(str, wc));
    TEST_ASSERT_INT_OP(==, -1, string_lastindex(str, ws));
    TEST_ASSERT_INT_OP(==, -1, string_clastindex(str, wc));
    TEST_ASSERT_INT_OP(==,  0, string_count(str, ws));
    TEST_ASSERT_INT_OP(==,  0, string_ccount(str, wc));

    TEST_ASSERT_FALSE(string_hasprefix(str, ws));
    TEST_ASSERT_FALSE(string_chasprefix(str, wc));
    TEST_ASSERT_FALSE(string_hassuffix(str, ws));
    TEST_ASSERT_FALSE(string_chassuffix(str, wc));
    TEST_ASSERT_FALSE(string_contains(str, ws));
    TEST_ASSERT_FALSE(string_ccontains(str, wc));
}

void test_index_empty(void)
{
    const char *wc = "";

    ws = string_ccopy(ws, wc);

    TEST_ASSERT_INT_OP(==, -1, string_index(str, ws));
    TEST_ASSERT_INT_OP(==, -1, string_cindex(str, wc));
    TEST_ASSERT_INT_OP(==, -1, string_lastindex(str, ws));
    TEST_ASSERT_INT_OP(==, -1, string_clastindex(str, wc));
    TEST_ASSERT_INT_OP(==,  0, string_count(str, ws));
    TEST_ASSERT_INT_OP(==,  0, string_ccount(str, wc));

    TEST_ASSERT_FALSE(string_hasprefix(str, ws));
    TEST_ASSERT_FALSE(string_chasprefix(str, wc));
    TEST_ASSERT_FALSE(string_hassuffix(str, ws));
    TEST_ASSERT_FALSE(string_chassuffix(str, wc));
    TEST_ASSERT_FALSE(string_contains(str, ws));
    TEST_ASSERT_FALSE(string_ccontains(str, wc));
}

void test_index_null(void)
{
    TEST_ASSERT_INT_OP(==, -1, string_index(str, NULL));
    TEST_ASSERT_INT_OP(==, -1, string_cindex(str, NULL));
    TEST_ASSERT_INT_OP(==, -1, string_lastindex(str, NULL));
    TEST_ASSERT_INT_OP(==, -1, string_clastindex(str, NULL));
    TEST_ASSERT_INT_OP(==,  0, string_count(str, NULL));
    TEST_ASSERT_INT_OP(==,  0, string_ccount(str, NULL));

    TEST_ASSERT_FALSE(string_hasprefix(str, NULL));
    TEST_ASSERT_FALSE(string_chasprefix(str, NULL));
    TEST_ASSERT_FALSE(string_hassuffix(str, NULL));
    TEST_ASSERT_FALSE(string_chassuffix(str, NULL));
    TEST_ASSERT_FALSE(string_contains(str, NULL));
    TEST_ASSERT_FALSE(string_ccontains(str, NULL));
}


int main(void)
{
    TEST_BEGIN();

    TEST_RUN(test_index_k);
    TEST_RUN(test_index_a);
    TEST_RUN(test_index_kay);
    TEST_RUN(test_index_ak_ka);
    TEST_RUN(test_index_boat);
    TEST_RUN(test_index_arobase);
    TEST_RUN(test_index_same_string);
    TEST_RUN(test_index_same_length_different_string);
    TEST_RUN(test_index_3kayak);
    TEST_RUN(test_index_empty);
    TEST_RUN(test_index_null);

    return TEST_END();
}
