#include "cstring.h"

#include <assert.h>
#include <string.h>
#include <stdlib.h>


struct string_s
{
    size_t cap;
    size_t len;
    char   buf[];
};


/* Accessors functions */

size_t string_cap(string_t s)
{
    assert(s != NULL);
    return s->cap;
}

size_t string_len(string_t s)
{
    assert(s != NULL);
    return s->len;
}

const char *string_buf(string_t s)
{
    assert(s != NULL);
    return s->buf;
}


/* Allocation functions */

#define string_free(s)          free(s)
#define string_malloc(size)     malloc(sizeof(struct string_s) + (size))
#define string_realloc(s, size) realloc(s, sizeof(struct string_s) + (size));

void string_delete(string_t s)
{
    string_free(s);
}

string_t string_make(size_t len)
{
    string_t new;

    new = string_malloc(len + 1);
    if (new != NULL)
    {
        new->cap    = len;
        new->len    = 0;
        new->buf[0] = 0;
    }

    return new;
}

string_t string_grow(string_t s, size_t len)
{
    string_t new;
    size_t cap;

    if (s == NULL || len <= s->cap)
        return s;

    cap = (s->cap != 0) ? s->cap : len;
    while (cap < len)
    {
        cap *= 2;
    }

    new = string_realloc(s, cap + 1);
    if (new != NULL)
    {
        new->cap = cap;
    }

    return new;
}


/* Manipulation functions */

static string_t string_internal__clone(const char *str, size_t len)
{
    string_t new;

    assert(str != NULL);

    new = string_make(len);
    if (new != NULL)
    {
        memcpy(new->buf, str, len);
        new->buf[len] = 0;
        new->len = len;
    }

    return new;
}

string_t string_clone(const string_t str)
{
    return (str != NULL) ? string_internal__clone(str->buf, str->len) : NULL;
}

string_t string_cclone(const char *str)
{
    return (str != NULL) ? string_internal__clone(str, strlen(str)) : NULL;
}

static string_t string_internal__copy(string_t s, const char *str, size_t len)
{
    string_t new;

    assert(s != NULL);
    assert(str != NULL);

    new = string_grow(s, len);
    if (new != NULL)
    {
        memcpy(new->buf, str, len);
        new->buf[len] = 0;
        new->len = len;
    }

    return new;
}

string_t string_copy(string_t s, const string_t str)
{
    assert(s != NULL);
    return (str != NULL) ? string_internal__copy(s, str->buf, str->len) : s;
}

string_t string_ccopy(string_t s, const char *str)
{
    assert(s != NULL);
    return (str != NULL) ? string_internal__copy(s, str, strlen(str)) : s;
}

static string_t string_internal__append(string_t s, const char *str, size_t len)
{
    string_t new;

    assert(s != NULL);
    assert(str != NULL);

    new = string_grow(s, s->len + len);
    if (new != NULL)
    {
        memcpy(new->buf + new->len, str, len);
        new->buf[new->len + len] = 0;
        new->len += len;
    }

    return new;
}

string_t string_append(string_t s, const string_t str)
{
    assert(s != NULL);
    return (str != NULL) ? string_internal__append(s, str->buf, str->len) : s;
}

string_t string_cappend(string_t s, const char *str)
{
    assert(s != NULL);
    return (str != NULL) ? string_internal__append(s, str, strlen(str)) : s;
}

static string_t string_internal__prepend(string_t s, const char *str, size_t len)
{
    string_t new;

    assert(s != NULL);
    assert(str != NULL);

    new = string_grow(s, s->len + len);
    if (new != NULL)
    {
        memmove(new->buf + len, new->buf, new->len);
        memcpy(new->buf, str, len);
        new->buf[new->len + len] = 0;
        new->len += len;
    }

    return new;
}

string_t string_prepend(string_t s, const string_t str)
{
    assert(s != NULL);
    return (str != NULL) ? string_internal__prepend(s, str->buf, str->len) : s;
}

string_t string_cprepend(string_t s, const char *str)
{
    assert(s != NULL);
    return (str != NULL) ? string_internal__prepend(s, str, strlen(str)) : s;
}

static string_t string_internal__join_sep_str(string_t s, const char *sep, size_t sep_len, const char *str, size_t str_len)
{
    string_t new;

    assert(s != NULL);
    assert(sep != NULL);
    assert(str != NULL);

    new = string_grow(s, s->len + sep_len + str_len);
    if (new != NULL)
    {
        memcpy(new->buf + new->len, sep, sep_len);
        memcpy(new->buf + new->len + sep_len, str, str_len);
        new->buf[new->len + sep_len + str_len] = 0;
        new->len += sep_len + str_len;
    }

    return new;
}

string_t string_join(string_t s, const string_t substrs[], size_t nmemb, const char *sep)
{
    size_t sep_len;

    assert(s != NULL);
    assert(substrs != NULL || nmemb == 0);
    assert(sep != NULL);

    if (nmemb == 0)
        return s;

    s = string_copy(s, substrs[0]);
    if (s == NULL)
        return NULL;

    sep_len = strlen(sep);

    for (size_t i = 1; i < nmemb; ++i)
    {
        s = string_internal__join_sep_str(s, sep, sep_len, substrs[i]->buf, substrs[i]->len);
        if (s == NULL)
            return NULL;
    }

    return s;
}

string_t string_cjoin(string_t s, const char *substrs[], size_t nmemb, const char *sep)
{
    size_t sep_len;

    assert(s != NULL);
    assert(substrs != NULL || nmemb == 0);
    assert(sep != NULL);

    if (nmemb == 0)
        return s;

    s = string_ccopy(s, substrs[0]);
    if (s == NULL)
        return NULL;

    sep_len = strlen(sep);

    for (size_t i = 1; i < nmemb; ++i)
    {
        s = string_internal__join_sep_str(s, sep, sep_len, substrs[i], strlen(substrs[i]));
        if (s == NULL)
            return NULL;
    }

    return s;
}

static string_t string_internal__replace(string_t s, const char *old, size_t old_len, const char *new, size_t new_len)
{
    size_t off;
    size_t growth;

    assert(s != NULL);
    assert(old != NULL);
    assert(new != NULL);

    if (old_len == 0)
        return s;

    growth = new_len > old_len ? new_len - old_len : 0;

    off = 0;
    while (off + old_len <= s->len)
    {
        if (memcmp(s->buf + off, old, old_len) != 0)
        {
            off++;
            continue;
        }

        s = string_grow(s, s->len + growth);
        if (s == NULL)
            return NULL;

        memmove(s->buf + off + new_len, s->buf + off + old_len, s->len - off);
        memcpy(s->buf + off, new, new_len);
        s->buf[s->len + new_len - old_len] = 0;
        s->len = s->len + new_len - old_len;

        off += new_len;
    }

    return s;
}

string_t string_replace(string_t s, const string_t old, const string_t new)
{
    assert(s != NULL);
    return (old != NULL && new != NULL) ? string_internal__replace(s, old->buf, old->len, new->buf, new->len) : s;
}

string_t string_creplace(string_t s, const char *old, const char *new)
{
    assert(s != NULL);
    return (old != NULL && new != NULL) ? string_internal__replace(s, old, strlen(old), new, strlen(new)) : s;
}


/* Processing functions */

string_t string_reset(string_t s)
{
    assert(s != NULL);
    s->buf[0] = 0;
    s->len = 0;
    return s;
}

string_t string_toupper(string_t s)
{
    char *ptr;

    assert(s != NULL);

    for (ptr = s->buf; *ptr != 0; ptr++)
    {
        if (*ptr >= 'a' && *ptr <= 'z')
        {
            int c = *ptr - 'a' + 'A';
            *ptr = (char)(c);
        }
    }

    return s;
}

string_t string_tolower(string_t s)
{
    char *ptr;

    assert(s != NULL);

    for (ptr = s->buf; *ptr != 0; ptr++)
    {
        if (*ptr >= 'A' && *ptr <= 'Z')
        {
            int c = *ptr - 'A' + 'a';
            *ptr = (char)(c);
        }
    }

    return s;
}

string_t string_trimright(string_t s, const char *chars)
{
    char *ptr;
    size_t idx;

    assert(s != NULL);
    assert(chars != NULL);

    ptr = s->buf + s->len - 1;
    idx = s->len;

    while (idx > 0 && strchr(chars, *ptr) != NULL)
    {
        ptr--;
        idx--;
    }

    s->len = idx;
    s->buf[idx] = 0;

    return s;
}

string_t string_trimleft(string_t s, const char *chars)
{
    char *ptr;
    size_t idx;

    assert(s != NULL);
    assert(chars != NULL);

    ptr = s->buf;
    idx = 0;

    while (idx < s->len && strchr(chars, *ptr) != NULL)
    {
        ptr++;
        idx++;
    }

    memmove(s->buf, ptr, s->len - idx);
    s->len -= idx;
    s->buf[s->len] = 0;

    return s;
}


/* Comparison functions */

int string_isempty(const string_t s)
{
    assert(s != NULL);
    return s->len == 0 && s->buf[0] == 0;
}

int string_compare(const string_t a, const string_t b)
{
    assert(a != NULL && b != NULL);
    return strncmp(a->buf, b->buf, a->len < b->len ? a->len : b->len);
}

int string_ccompare(const string_t a, const char *b)
{
    size_t len;
    assert(a != NULL && b != NULL);
    len = strlen(b);
    return strncmp(a->buf, b, a->len < len ? a->len : len);
}


/* Index based functions */

static int string_internal__index_from(const string_t s, size_t off, const char *substr, size_t len)
{
    assert(s != NULL);
    assert(substr != NULL);

    if (len == 0)
        return -1;

    while (off + len <= s->len)
    {
        if (memcmp(s->buf + off, substr, len) == 0)
            return (int)(off);

        off++;
    }

    return -1;
}

int string_index(const string_t s, const string_t substr)
{
    assert(s != NULL);
    return (substr != NULL) ? string_internal__index_from(s, 0, substr->buf, substr->len) : -1;
}

int string_cindex(const string_t s, const char *substr)
{
    assert(s != NULL);
    return (substr != NULL) ? string_internal__index_from(s, 0, substr, strlen(substr)) : -1;
}

static int string_internal__lastindex(const string_t s, const char *substr, size_t len)
{
    size_t off;

    assert(s != NULL);
    assert(substr != NULL);

    if (len == 0 || s->len < len)
        return -1;

    off = s->len - len;
    do
    {
        if (memcmp(s->buf + off, substr, len) == 0)
            return (int)(off);
    } while (off-- > 0);

    return -1;
}

int string_lastindex(const string_t s, const string_t substr)
{
    assert(s != NULL);
    return (substr != NULL) ? string_internal__lastindex(s, substr->buf, substr->len) : -1;
}

int string_clastindex(const string_t s, const char *substr)
{
    assert(s != NULL);
    return (substr != NULL) ? string_internal__lastindex(s, substr, strlen(substr)) : -1;
}

int string_hassuffix(const string_t s, const string_t substr)
{
    int index;

    assert(s != NULL);

    if (substr == NULL)
        return 0;

    index = string_lastindex(s, substr);

    return (index != -1) ? (size_t)(index) == s->len - substr->len : 0;
}

int string_chassuffix(const string_t s, const char *substr)
{
    int index;

    assert(s != NULL);

    if (substr == NULL)
        return 0;

    index = string_clastindex(s, substr);

    return (index != -1) ? (size_t)(index) == s->len - strlen(substr) : 0;
}

static int string_internal__count(const string_t s, const char *substr, size_t len)
{
    int index;
    int occurences = 0;
    size_t offset = 0;

    assert(s != NULL);
    assert(substr != NULL);

    while ((index = string_internal__index_from(s, offset, substr, len)) != -1)
    {
        occurences++;
        offset = (size_t)(index) + len;
    }

    return occurences;
}

int string_count(const string_t s, const string_t substr)
{
    assert(s != NULL);
    return (substr != NULL) ? string_internal__count(s, substr->buf, substr->len) : 0;
}

int string_ccount(const string_t s, const char *substr)
{
    assert(s != NULL);
    return (substr != NULL) ? string_internal__count(s, substr, strlen(substr)) : 0;
}
