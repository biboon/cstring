#ifndef __CSTRING_H__
#define __CSTRING_H__

#include <stddef.h>

typedef struct string_s *string_t;

/* Accessors functions */
size_t       string_cap(string_t s);
size_t       string_len(string_t s);
const char * string_buf(string_t s);

/* Allocation functions */
void     string_delete(string_t s);
string_t string_make(size_t len);
string_t string_grow(string_t s, size_t len);

/* Manipulation functions */
string_t string_clone(const string_t s);
string_t string_cclone(const char *str);

string_t string_copy(string_t s, const string_t str);
string_t string_ccopy(string_t s, const char *str);
string_t string_append(string_t s, const string_t str);
string_t string_cappend(string_t s, const char *str);
string_t string_prepend(string_t s, const string_t str);
string_t string_cprepend(string_t s, const char *str);

string_t string_join(string_t s, const string_t substrs[], size_t nmemb, const char *sep);
string_t string_cjoin(string_t s, const char *substrs[], size_t nmemb, const char *sep);

string_t string_replace(string_t s, const string_t old, const string_t new);
string_t string_creplace(string_t s, const char *old, const char *new);


/* Processing functions */
string_t string_reset(string_t s);
string_t string_toupper(string_t s);
string_t string_tolower(string_t s);
string_t string_trimright(string_t s, const char *chars);
string_t string_trimleft(string_t s, const char *chars);

static inline string_t string_trim(string_t s, const char *chars)
{
    return string_trimleft(string_trimright(s, chars), chars);
}

static inline string_t string_trimspace(string_t s)
{
    return string_trim(s, " \f\n\r\t\v");
}

/* Comparison functions */
int string_isempty(const string_t s);
int string_compare(const string_t a, const string_t b);
int string_ccompare(const string_t a, const char *b);

static inline int string_equals(const string_t a, const string_t b)
{
    return !string_compare(a, b);
}

static inline int string_cequals(const string_t a, const char *b)
{
    return !string_ccompare(a, b);
}

/* Index based functions */
int string_index(const string_t s, const string_t substr);
int string_cindex(const string_t s, const char *substr);
int string_lastindex(const string_t s, const string_t substr);
int string_clastindex(const string_t s, const char *substr);
int string_hassuffix(const string_t s, const string_t substr);
int string_chassuffix(const string_t s, const char *substr);
int string_count(const string_t s, const string_t substr);
int string_ccount(const string_t s, const char *substr);

static inline int string_contains(const string_t s, const string_t substr)
{
    return !!(string_index(s, substr) + 1);
}

static inline int string_ccontains(const string_t s, const char *substr)
{
    return !!(string_cindex(s, substr) + 1);
}

static inline int string_hasprefix(const string_t s, const string_t substr)
{
    return string_index(s, substr) == 0;
}

static inline int string_chasprefix(const string_t s, const char *substr)
{
    return string_cindex(s, substr) == 0;
}

#endif /* __CSTRING_H__ */
