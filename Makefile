ifndef VERBOSE
.SILENT:
define tell
	echo "\033[0;36m$(1)\033[0m"
endef
endif


CFLAGS := -std=c99 -D_DEFAULT_SOURCE -Wall -Wextra
CFLAGS += -Wshadow -Wpointer-arith -Wvla -Wdeclaration-after-statement
CFLAGS += -Wconversion -Wcast-qual -Winit-self -Wold-style-definition
CFLAGS += -Wmissing-field-initializers -Wno-unknown-pragmas -Wundef
CFLAGS += -Wcast-align -Wstrict-prototypes -Wmissing-prototypes
CFLAGS += -Wwrite-strings -Wswitch-default -Wunreachable-code


default: tests

tests: $(patsubst test/%.c,bin/test/%.out,$(wildcard test/*.c))
	$(call tell,"Running tests...")
	$(foreach f,$^,./$(f);)

clean:
	rm -rf bin build


bin/test/%.out: build/src/cstring.o build/test/%.o
	@mkdir -p $(@D)
	$(CC) $(LDFLAGS) -o $@ $^


.PRECIOUS: build/src/%.o build/test/%.o

build/src/%.o: src/%.c src/%.h
	@mkdir -p $(@D)
	$(call tell,"Building object $@")
	$(CC) $(CFLAGS) -c -o $@ $< -Isrc

build/test/%.o: test/%.c src/cstring.h CUTe/cute.h
	@mkdir -p $(@D)
	$(call tell,"Building object $@")
	$(CC) $(CFLAGS) -Wno-missing-prototypes -c -o $@ $< -Isrc -ICUTe
